![Screenshot](https://raw.githubusercontent.com/Vitexus/netbeans.deb/master/netbeans.png)

Apache Netbeans Debian package
==============================

Repacked binary of https://github.com/apache/incubator-netbeans ready to use with Debian


Installation
------------

```
wget -O - http://v.s.cz/info@vitexsoftware.cz.gpg.key|sudo apt-key add -
echo deb http://v.s.cz/ stable main > /etc/apt/sources.list.d/ease.list
apt update
apt install netbeans
```

or 

```
igdebi https://www.vitexsoftware.cz/pool/main/n/netbeans/netbeans_10.0-1_all.deb
```



![Screenshot](https://raw.githubusercontent.com/Vitexus/netbeans.deb/master/screenshot.png)



See also
--------

 * https://github.com/VitexSoftware/netbeans-php-tools
